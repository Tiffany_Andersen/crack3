/*  Tiffany Andersen
    CSCI 46
    November 22, 2021   
    
    This program will read in a file of passwords and hash them, creating objects that contain
    the password and the hash, and storing those objects in an array.  It then opens a file of hashes
    and searches the array for those hashes.  If a match is found, it will print the password and hash. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;   // Maximum any password will be

int ecompare(const void *a, const void *b);
int esearch(const void *t, const void *e);


// Stucture to hold both a plaintext password and a hash.
typedef struct
{
    char plain[50];
    char hash[33];
} entry;

//print function to test array build
void printEntry(const entry *e)
{
    printf("%s %s", e->plain, e->hash);
}

// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary word.
entry *read_dictionary(char *filename, int *size)
{
    // open dictionary file
    FILE *f = fopen(filename, "r");
    if (!f)
    {
        fprintf(stderr, "Can't open %s for reading\n", filename);
        perror(NULL);
        exit(1);
    }

    //allocate space for the array
    entry *entries = malloc(100 * sizeof(entry));
    
    // create a variable to count entries
    int count = 0;
    
    // load in each word and hash it
    char word[100];
    int capacity = 100; //variable to add more to array when needed

    while(fgets(word, 100, f) != NULL)
    {
        //trim newline if needed
        char *nl = strchr(word, '\n');
        if (nl) *nl = '\0';

        //hash the word that was just read in
        char *hashed = md5(word, strlen(word));
        
        //add word to entry, store in array
        sscanf(word, "%s", entries[count].plain);
        //add hash to entry, store in array
        sscanf(hashed, "%s", entries[count].hash);
        

        count++;

        //when we need to expand the array:
        if (count == capacity)
        {
            //make array bigger
            capacity += 1000;
            //assign bigger array back to entries
            entries = realloc(entries, capacity * sizeof(entry));
        }

        //free up space
        free(hashed);
    }
    //close the file
    fclose(f);
    
    // increment size
    *size = count;
    // return pointer to array that was built
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int size;
    // Read the dictionary file into an array of entry structures
    entry *dict = read_dictionary(argv[2], &size);

    // Sort the hashed dictionary using qsort.
    qsort(dict, size, sizeof(entry), ecompare);

    //print array to make sure it's working
    /*for (int i = 0; i < size; i++)
    {
        printf("%d: ",i);
        printEntry(&dict[i]);
        printf("\n");
    }*/

    // Open the hash file for reading. 
    FILE *ht = fopen(argv[1], "r");
    if (!ht)
    {
        fprintf(stderr, "Can't open hashes.txt for reading\n");
        perror(NULL);
        exit(1);
    }

    // For each hash, search for it in the dictionary using
    // binary search.
    char tryhash[33];   //to store each hash
    
    //variable to see how many passwords were cracked in each file
    int foundits = 0;
    while (fgets(tryhash, 33, ht) != NULL)
    {
        entry *foundhash = bsearch(tryhash, dict, size, sizeof(entry), esearch);
        // get the corresponding plaintext dictionary word.
        // Print out both the hash and word.
        if(foundhash)
        {
            foundits++;
            printf("%d) Found it! ", foundits);
            printEntry(foundhash);
            printf("\n");
        }
    }

    fclose(ht);
    free(dict);
}

// ecompare to use with qsort
int ecompare(const void *a, const void *b)
{
    entry *aa = (entry *)a;
    entry *bb = (entry *)b;

    return strcmp(aa->hash, bb->hash);
}

//esearch to use with bsearch
int esearch(const void *t, const void *e)
{
    char *tt = (char *)t;
    entry *ee = (entry *)e;

    return strcmp(tt, ee->hash);
}
